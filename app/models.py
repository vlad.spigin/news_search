
from typing import List, Optional
from pydantic import BaseModel, Field


class Synonym(BaseModel):
    word: str = Field(...)
    synonyms: List[str] = Field(...)

class Article(BaseModel):
    title: str = Field(...)
    description: Optional[str] = Field(None)
    url: str = Field(...)

class SearchQuery(BaseModel):
    query: str = Field(...)
    synonyms: Optional[List[str]] = Field([])
