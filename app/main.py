from fastapi import FastAPI
from routes import router

app = FastAPI()

@app.on_event("startup")
async def startup():
    print("Connecting to database...")

@app.on_event("shutdown")
async def shutdown():
    print("Disconnecting from database...")

app.include_router(router)