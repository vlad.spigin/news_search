from models import Synonym, Article, SearchQuery
from typing import List

def get_synonyms(word:str) -> list[str]:
    return ['synonim1', 'synonim2', 'synonim3']

def search_articles(query:SearchQuery) -> List[Article]:
    return [Article(title='testTitle', description='testDescription', url='testUrl'),]