from fastapi import APIRouter, HTTPException
from typing import List
from services import get_synonyms, search_articles
from models import Synonym, Article, SearchQuery

router = APIRouter()

@router.get("/synonyms/{word}", response_model=Synonym)
async def get_synonyms_route(word: str):
    synonyms: list[str] = get_synonyms(word)
    if not synonyms:
        raise HTTPException(status_code=404, detail="Synonyms not found")
    return Synonym(word=word, synonyms=synonyms)

@router.get("/search/{query}", response_model=List[Article])
async def search_articles_route(query: str, synonyms: List[str] = []):
    search_query = SearchQuery(query=query, synonyms=synonyms)
    articles = search_articles(search_query)
    return articles